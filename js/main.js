$('[data-toggle="slide-collapse"]').on('click', function() {
  $navMenuCont = $($(this).data('target'));
  $navMenuCont.animate({
    'width': 'toggle'
  }, 350);
  $(".menu-overlay").fadeIn(500);

});
$(".menu-overlay").click(function(event) {
  $(".navbar-toggle").trigger("click");
  $(".menu-overlay").fadeOut(500);
});

var navigation = {

	fadeSpeed : 200,
	scrollSpeed : 500,

	_scrolled : false,

	init : function() {
		var _this = this;
		var $backToTop = $("#main-footer .back-top");

		// Back to top button behaviour
		$backToTop.click(function(event) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop : 0
			}, _this.scrollSpeed);

		})

		// Hide and show on scroll
		$(window).scroll(function(event) {

			// Incentive arrow
			if (!_this._scrolled) {
				$('#main-footer .scroll-incentive').fadeOut(_this.fadeSpeed);
				_this._scrolled = true;
			}

			// Back to top button
			if ($('html, body').scrollTop() > 0) {
				$backToTop.fadeIn(_this.fadeSpeed);
			} else {
				$backToTop.fadeOut(_this.fadeSpeed);
			}
		});
	}
}

navigation.init();

