<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- PVP Tables -->
            <div class="col-md-12">
                <!-- Top Killers -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title">Top Killers</h2>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Race</th>
                                <th scope="col">Class</th>
                                <th scope="col">Level</th>
                                <th scope="col">Total Kills</th>
                            </tr>
                            </thead>
                            <tbody>
						        <?php

                                // This will sort results by totalKills
                                $topkillers = $mysqliC->query("SELECT * FROM `characters` ORDER BY totalKills desc") or die (mysqli_error($mysqliC));

                                if (mysqli_num_rows($topkillers) > 0)
                                {
                                    while($data = $topkillers->fetch_assoc())
                                    {
                                        echo 
                                        '
                                            <tr>
                                                <td class="class-'. $data['class'].'"> '. $data['name'] . '</td>
                                                <td><img src="images/races/'. $data['race'] .'_'. $data['gender'] .'.png" width="16" height="16" alt="race"></td>
                                                <td><img src="images/classes/'. $data['class'] .'.png" width="16" height="16" alt="class"></td>
                                                <td>'. $data['level'] .'</td>
                                                <td>'. $data['totalKills'] .'</td>
                                            </tr>
                                        ';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- End of Top Killers -->
                <!-- Top Killers of Today -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title">Top Killers of Today</h2>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Race</th>
                                <th scope="col">Class</th>
                                <th scope="col">Level</th>
                                <th scope="col">Kills Today</th>
                            </tr>
                            </thead>
                            <tbody>
						        <?php

                                // This will sort results by todayKills
                                $topkillers = $mysqliC->query("SELECT * FROM `characters` ORDER BY todayKills desc") or die (mysqli_error($mysqliC));

                                if (mysqli_num_rows($topkillers) > 0)
                                {
                                    while($data = $topkillers->fetch_assoc())
                                    {
                                        echo 
                                        '
                                            <tr>
                                                <td class="class-'. $data['class'].'"> '. $data['name'] . '</td>
                                                <td><img src="images/races/'. $data['race'] .'_'. $data['gender'] .'.png" width="16" height="16" alt="race"></td>
                                                <td><img src="images/classes/'. $data['class'] .'.png" width="16" height="16" alt="class"></td>
                                                <td>'. $data['level'] .'</td>
                                                <td>'. $data['todayKills'] .'</td>
                                            </tr>
                                        ';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- End of Top Killers of Today -->
                <!-- Top Honor Points -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title">Top Honor Points</h2>

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Race</th>
                                <th scope="col">Class</th>
                                <th scope="col">Level</th>
                                <th scope="col">Honor Points </th>
                                <th scope="col">Total Kills</th>
                            </tr>
                            </thead>
                            <tbody>
						        <?php

                                // This will sort results by honor Points
                                $topkillers = $mysqliC->query("SELECT * FROM `characters` ORDER BY honor desc") or die (mysqli_error($mysqliC));

                                if (mysqli_num_rows($topkillers) > 0)
                                {
                                    while($data = $topkillers->fetch_assoc())
                                    {
                                        echo 
                                        '
                                            <tr>
                                                <td class="class-'. $data['class'].'"> '. $data['name'] . '</td>
                                                <td><img src="images/races/'. $data['race'] .'_'. $data['gender'] .'.png" width="16" height="16" alt="race"></td>
                                                <td><img src="images/classes/'. $data['class'] .'.png" width="16" height="16" alt="class"></td>
                                                <td>'. $data['level'] .'</td>
                                                <td>'. $data['honor'] .'</td>
                                                <td>'. $data['totalKills'] .'</td>
                                            </tr>
                                        ';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- End of top honor points -->
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>