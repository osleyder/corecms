<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-coins"></i> Buy coins</h2>
                        <?php
                        if(isset($_SESSION['id']))
                        {
                            $bnetID = $_SESSION['id'];
                            $get_accID = $mysqliA->query("SELECT * FROM `account` WHERE `battlenet_account` = '$bnetID';") or die (mysqli_error($mysqliA));
                            while($bnet_ress = $get_accID->fetch_assoc())
                            {
                                $accountID = $bnet_ress['id'];
                            }

                            ?>
                            <?php if ($site_don == 1) { ?>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card mb-5 mb-lg-0" id="card-wow">
                                        <div class="card-body">
                                            <h2 class="card-title text-warning text-center"><i class="fad fa-star"></i></h2>
                                            <h5 class="card-title text-warning text-center">Tier 1</h5>
                                            <h6 class="card-price text-center">5<span class="period"><?php if($paypal_currency == "EUR") {echo '&euro;';} if($paypal_currency == "USD") { echo '$';} ?></span></h6>
                                            <hr>
                                            <ul class="fa-ul">
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span><span class="badge badge-warning">200 <i class="fad fa-coin"></i></span></li>
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Bonus <span class="badge badge-warning">10 <i class="fad fa-coin"></i></span></li>
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Instant delivery</li>
                                            </ul>
                                            <form name="tier1" method="post" action="buy.php">
                                                <input type="hidden" name="amount" value="5">
                                                <!-- payment button. -->
                                                <button type="submit" name="pay_now" id="pay_now" class="btn btn-block btn-primary">Buy with <i class="fab fa-paypal"></i> PayPal</button>
                                            </form>
                                            <small class="text-center">Note! PayPal fee is not included!</small>
                                        </div>
                                    </div>
                                </div>
                                <!-- Plus Tier -->
                                <div class="col-lg-4">
                                    <div class="card mb-5 mb-lg-0" id="card-wow">
                                        <div class="card-body">
                                            <h2 class="card-title text-warning text-uppercase text-center"><i class="fad fa-star"></i> <i class="fad fa-star"></i></h2>
                                            <h5 class="card-title text-warning text-center">Tier 2</h5>
                                            <h6 class="card-price text-center">10<span class="period"><?php if($paypal_currency == "EUR") {echo '&euro;';} if($paypal_currency == "USD") { echo '$';} ?></span></h6>
                                            <hr>
                                            <ul class="fa-ul">
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span><span class="badge badge-warning">500 <i class="fad fa-coin"></i></span></li>
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Bonus <span class="badge badge-warning">50 <i class="fad fa-coin"></i></span></li>
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Instant delivery</li>
                                            </ul>
                                            <form name="tier1" method="post" action="buy.php">
                                                <input type="hidden" name="amount" value="10">
                                                <!-- payment button. -->
                                                <button type="submit" name="pay_now" id="pay_now" class="btn btn-block btn-primary">Buy with <i class="fab fa-paypal"></i> PayPal</button>
                                            </form>
                                            <small class="text-center">Note! PayPal fee is not included!</small>
                                        </div>
                                    </div>
                                </div>
                                <!-- Pro Tier -->
                                <div class="col-lg-4">
                                    <div class="card" id="card-wow">
                                        <div class="card-body">
                                            <h2 class="card-title text-warning text-uppercase text-center"><i class="fad fa-star"></i><i class="fad fa-star"></i><i class="fad fa-star"></i></h2>
                                            <h5 class="card-title text-warning text-center">Tier 3</h5>
                                            <h6 class="card-price text-center">15<span class="period"><?php if($paypal_currency == "EUR") {echo '&euro;';} if($paypal_currency == "USD") { echo '$';} ?></span></h6>
                                            <hr>
                                            <ul class="fa-ul">
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span><span class="badge badge-warning">1000 <i class="fad fa-coin"></i></span></li>
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Bonus <span class="badge badge-warning">100 <i class="fad fa-coin"></i></span></li>
                                                <li><span class="fa-li"><i class="fas fa-check"></i></span>Instant delivery</li>
                                            </ul>
                                            <form name="tier1" method="post" action="buy.php">
                                                <input type="hidden" name="amount" value="15">
                                                <!-- payment button. -->
                                                <button type="submit" name="pay_now" id="pay_now" class="btn btn-block btn-primary">Buy with <i class="fab fa-paypal"></i> PayPal</button>
                                            </form>
                                            <small class="text-center">Note! PayPal fee is not included!</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="alert alert-warning progress-bar-animated progress-bar-striped">
                                <i class="fad fa-exclamation-triangle"></i> We don't do refunds!
                            </div>
                            <?php } elseif( $site_don == 0) { ?>
                                <div class="alert alert-warning progress-bar-animated progress-bar-striped">
                                <i class="fad fa-exclamation-triangle"></i> This feature was disabled by the administrator
                            </div>
                            <?php } ?>
                            <?php
                        }
                        else
                        {
                            echo '
                            <div class="alert alert-info">
                                <i class="fad fa-exclamation-circle"></i> You need to be logged in to buy coins!<br /><br />
                                <i class="fad fa-spinner-third fa-spin"></i> Redirecting to login page...
                            </div>
                            ';
                            header("refresh:3; url=$custdir/login.php");
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>